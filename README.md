# Visitor Sign In System (VSS) Frontend

The Visitor Sign In System (VSS for short) aim to welcome visitors, and allow them to indicate their arrival. The frontend  is built in Vue.js and is optimised for the raspberry pi official touchscreen (800*480px). 

 
## Project setup

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
